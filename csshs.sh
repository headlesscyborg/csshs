#!/bin/bash
# Cloud SSH Sync
# 2019-2020 | "headless_cyborg"
#####################################################################
#This program is free software: you can redistribute it and/or modify
#it under the terms of the GNU General Public License as published by
#the Free Software Foundation, either version 3 of the License, or
#(at your option) any later version.

#This program is distributed in the hope that it will be useful,
#but WITHOUT ANY WARRANTY; without even the implied warranty of
#MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#GNU General Public License for more details.

#You should have received a copy of the GNU General Public License
#along with this program. If not, see <http://www.gnu.org/licenses/>.
#####################################################################
# NOTE: It's recommended to set up an SSH KEY-based autorization, otherwise rsync will be asking for password - ALWAYS

# COLOR DEFINITIONS
# cl = text color only; bg = background color only; bo = behaviour option
cl_darkred="\e[31m"
cl_magneta="\e[35m"
cl_lmag="\e[95m"
cl_cyan="\e[36m"
cl_lcyan="\e[96m"
cl_lred="\e[91m"
cl_lgreen="\e[92m"
cl_clear="\e[0m"
bg_red="\e[41m"
bg_magneta="\e[45m"
bg_lgreen="\e[102m"
bg_red="\e[41m"
bo_blink="\e[5m"

# CONFIGURATION
file_config="csshs_config"
source $file_config

# CONFIGURATION FILE CHECK
function config_file_check()
  {
    echo -e "$cl_lcyan[CSSHS]$cl_cyan Checking configuration file"
    error_found=0
    if [ -e $file_config ]; then
      if grep -q "server=" "$file_config";
  			then
  				echo -e "$cl_cyan Config file check - server: $server $cl_lgreen[✔]$cl_clear"
  			else
  				echo -e "$cl_cyan Config file check - server: not set $cl_lred[x]$cl_clear"
          error_found=1
  		fi

  		if grep -q port= "$file_config";
  			then
  				echo -e "$cl_cyan Config file check - port: $port $cl_lgreen[✔]$cl_clear"
  			else
  				echo -e "$cl_cyan Config file check - port: not set $cl_lred[x]$cl_clear"
          error_found=1
  		fi

  		if grep -q user= "$file_config";
  			then
  				echo -e "$cl_cyan Config file check - user: $user $server $cl_lgreen[✔]$cl_clear"
  			else
  				echo -e "$cl_cyan Config file check - user: not set $cl_lred[x]$cl_clear"
          error_found=1
  		fi

  		if grep -q sec= "$file_config";
  			then
  				echo -e "$cl_cyan Config file check - sec: $sec $server $cl_lgreen[✔]$cl_clear"
  			else
  				echo -e "$cl_cyan Config file check - sec: not set $cl_lred[x]$cl_clear"
          error_found=1
        fi

      if grep -q sync_folders= "$file_config";
    		then
    				echo -e "$cl_cyan Config file check - number of sync folders: ${#sync_folders[@]}"
            echo -e "$cl_cyan Config file check - specified sync folders: ${sync_folders[@]}"
    		else
    				echo -e "$cl_cyan Config file check - sync_folders: not set $cl_lred[x]$cl_clear"
            error_found=1
        fi

        if grep -q target_folder= "$file_config";
    			then
    				echo -e "$cl_cyan Config file check - target_folder: $target_folder $server $cl_lgreen[✔]$cl_clear"
    			else
    				echo -e "$cl_cyan Config file check - target_folder: not set $cl_lred[x]$cl_clear"
            error_found=1
          fi
    else
        echo -e "$cl_cyan Config file check - file doesn't exist $cl_lred[x]$cl_clear"
        error_found=1
    fi

    if [ $error_found -eq "1" ]; then
      echo -e "$cl_cyan Configuration file doesn't meet the expectations, exiting."
      exit
    else
      echo -e "$cl_cyan Configuration file is ok $server $cl_lgreen[✔]$cl_clear"
    fi
  }

function sync_folder_check()
  {
    error_found=0
    echo -e "$cl_lcyan[CSSHS]$cl_cyan Checking sync_folders"
    for d in "${sync_folders[@]}"; do
      if [ -d "$d" ]; then
          echo -e "$cl_cyan Folder $d exists $server $cl_lgreen[✔]$cl_clear"
      else
          echo -e "$cl_cyan Folder $d doesn't exist $cl_lred[x]$cl_clear"
          error_found=1
      fi
  done

  if [ $error_found -eq "1" ]; then
    echo -e "$cl_lcyan[CSSHS]$cl_cyan Some of the directories specified as sync_folders don't exist."
    exit
  else
    echo -e "$cl_lcyan[CSSHS]$cl_cyan sync_folders ok."
  fi
  }

function anti-root_check()
  {
    if [[ $EUID -eq 0 ]]; then
    	echo -e "$cl_lcyan[CSSHS]$cl_cyan It's not recommended to run this script as ROOT.$cl_clear"
    	exit
    else
      :
    fi
  }

function ping_check()
  {
      echo -e "$cl_lcyan[CSSHS]$cl_cyan Performing ping check"
    	if [ "`ping -c 1 $server`" ]; then
        echo -e "$cl_cyan Ping check $cl_lgreen[✔]$cl_clear"
      else
        echo -e "$cl_cyan Ping check $cl_lred[x]$cl_clear"
        echo -e "$cl_lcyan[CSSHS]$cl_cyan Server seems to be unavailable, waiting..."
        sleep 60
        ping_check
      fi
  }

function sync()
  {
    echo -e "$cl_lcyan[CSSHS]$cl_cyan$bo_blink Starting in 3 seconds, press CTRL+C to cancel$cl_clear$cl_cyan"
    sleep 3
    $default_command ${sync_folders[@]} -e "ssh -q -p $port" $user@$server:$target_folder
    echo -e "$cl_lcyan[CSSHS]$cl_cyan Sync completed on $(date), the next sync will happen $(date -d "$sec seconds")$cl_clear"
		sleep 1
	  echo -e "$cl_lcyan[CSSHS]$cl_cyan Waiting $sec seconds$cl_clear"
		sleep $sec
		ping_check
		sync
  }

default_command="rsync --prune-empty-dirs --update --no-motd --progress --human-readable --preallocate --stats --delete-before -r"
echo -e "$cl_darkred############################################$cl_clear"
echo -e "$cl_darkred#             Cloud SSH Sync               #$cl_clear"
echo -e "$cl_darkred# Secure Shell synchronisation utility     #$cl_clear"
echo -e "$cl_darkred# Author: headless_cyborg                  #$cl_clear"
echo -e "$cl_darkred# Version: 0.6                             #$cl_clear"
echo -e "$cl_darkred# Dev branch: f                            #$cl_clear"
echo -e "$cl_darkred############################################$cl_clear"
# NOTE: Do not use experimental ('e') branch version if you don't have a backup of your files
	echo -e ""
  anti-root_check
  config_file_check
  sync_folder_check
  ping_check
  sync
