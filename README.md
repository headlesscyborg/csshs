CSSHS - Cloud Secure Shell Synchronisation

[DESCRIPTION]
- CSSHS is a simple bash script based on rsync used for automatic backup of a set of folders to a remote SSH server.

[HOW TO USE]
- Verify that you have rsync installed on the client machine
- Set-up a key-based SSH authorization between the client and the remote server
- Adjust the "csshs_config" configuration file to your needs (server, port, username etc.)
- Run ./csshs.sh and the script will do the job

[REQUIREMENTS]
- bash >= 3.0
- rsync ~3.1.3

[WHAT HAPPENS IF]
- ... a folder that is supposed to be synced is deleted and CSSHS keeps running? CSSHS will skip such a folder so in case you lose all your files / disk drive on the client machine, they will still be available on the server.
- ... a file in a folder that is supposed to be synced is deleted and CSSHS keeps running? Such a file will be deleted on the remote server in the next sync process, CSSHS mirrors the changes.

[DISCLAIMER]
-  The script is provided without any guarantee, only use at your own risk, potential damage in case of wrong usage can lead to a disaster and the creator is not responsible for such situations.